package sping

import constants.BASE_TASK_URL
import constants.TestMessages.INCORRECT_CODE_MESSAGE
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.client.RestTemplate
import org.testng.Assert
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test
import test.rest.client.RestTemplateClient
import test.rest.models.user.User

class RestTemplateTest {
    private var baseClient: RestTemplate? = null

    @BeforeClass
    fun setUpRestTemplate() {
        baseClient = RestTemplateClient.getBaseClient()
    }

    @Test
    fun successfulStatusCodeTest() {
        val responseEntity = baseClient!!.getForEntity(BASE_TASK_URL, Array<User>::class.java)
        Assert.assertEquals(responseEntity.statusCode, HttpStatus.OK, INCORRECT_CODE_MESSAGE)
    }

    @Test
    fun headerTest() {
        val responseEntity = baseClient!!.getForEntity(BASE_TASK_URL, Array<User>::class.java)
        val httpHeaders = responseEntity.headers
        Assert.assertTrue(httpHeaders.containsKey(HttpHeaders.CONTENT_TYPE), "There is no Content-Type header.")
        Assert.assertEquals(httpHeaders.contentType, MediaType.APPLICATION_JSON_UTF8, "Not valid content type.")
    }

    @Test
    fun userCountTest() {
        val users = baseClient!!.getForEntity(BASE_TASK_URL, Array<User>::class.java)
        Assert.assertEquals(users.statusCode, HttpStatus.OK, INCORRECT_CODE_MESSAGE)
        Assert.assertEquals(users.body.size, 10, "Not valid users count.")
    }
}
