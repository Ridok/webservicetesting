package jayway

import com.jayway.restassured.RestAssured.given
import com.jayway.restassured.http.ContentType
import constants.BASE_TASK_URL
import org.testng.Assert
import org.testng.annotations.Test
import test.rest.models.user.User

class RestAssuredTest {
    @Test
    fun successfulStatusCodeTest() {
        given().`when`().get(BASE_TASK_URL).then().statusCode(200)
    }

    @Test
    fun headerTest() {
        val response = given().`when`().get(BASE_TASK_URL)
        val headers = response.headers
        Assert.assertTrue(headers.hasHeaderWithName("Content-Type"), "There is no header Content-Type.")
        response.then().contentType(ContentType.JSON)
    }

    @Test
    fun userCountTest() {
        val response = given().`when`().get(BASE_TASK_URL)
        response.then().statusCode(200)
        val users = response.`as`(Array<User>::class.java)
        Assert.assertEquals(users.size, 10, "Not valid users count.")
    }
}
