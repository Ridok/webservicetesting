package constants

object TestMessages {
    const val INCORRECT_CODE_MESSAGE = "No valid status code."
}

const val BASE_TASK_URL = "https://jsonplaceholder.typicode.com/users"
const val BONUS_TASK_URL = "https://api.github.com/gists"
