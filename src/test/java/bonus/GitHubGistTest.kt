package bonus

import constants.BONUS_TASK_URL
import constants.TestMessages
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.web.client.RestTemplate
import org.testng.Assert
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test
import test.rest.client.RestTemplateClient
import test.rest.models.gist.File
import test.rest.models.gist.Gist
import test.rest.models.gist.GistResponse

class GitHubGistTest {
    private var baseClient: RestTemplate? = null

    private val baseGist: Gist
        get() {
            val file = File("Hello world!")
            return Gist("Made by automation test.", mapOf("helloWorld.txt" to file), true)
        }

    @BeforeClass
    fun setUpBaseClient() {
        baseClient = RestTemplateClient.getGistBaseClient()
    }

    @Test
    fun gistTest() {
        val postResponse = baseClient!!.postForEntity(BONUS_TASK_URL, baseGist, GistResponse::class.java)
        Assert.assertEquals(postResponse.statusCode, HttpStatus.CREATED, TestMessages.INCORRECT_CODE_MESSAGE)

        val getResponse = baseClient!!.getForEntity("$BONUS_TASK_URL/${postResponse.body.id}", Gist::class.java)
        Assert.assertEquals(getResponse.statusCode, HttpStatus.OK, TestMessages.INCORRECT_CODE_MESSAGE)
        Assert.assertEquals(getResponse.body, baseGist, "Incorrect body.")

        val patchResponse = baseClient!!.exchange("$BONUS_TASK_URL/${postResponse.body.id}",
                HttpMethod.PATCH, HttpEntity(baseGist), GistResponse::class.java)
        Assert.assertEquals(patchResponse.statusCode, HttpStatus.OK, TestMessages.INCORRECT_CODE_MESSAGE)

        val deleteResponse = baseClient!!.exchange("$BONUS_TASK_URL/${postResponse.body.id}",
                HttpMethod.DELETE, HttpEntity(""), String::class.java)
        Assert.assertEquals(deleteResponse.statusCode, HttpStatus.NO_CONTENT, TestMessages.INCORRECT_CODE_MESSAGE)
    }
}
