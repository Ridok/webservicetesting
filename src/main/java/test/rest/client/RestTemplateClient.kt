package test.rest.client

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import java.util.ArrayList
import org.apache.http.Header
import org.apache.http.client.config.RequestConfig
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.message.BasicHeader
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.client.RestTemplate
import utils.propertiesreader.PropertiesReader.getPropertyValue

object RestTemplateClient {
    private val log: Logger = LoggerFactory.getLogger(RestTemplateClient.javaClass)

    private const val TIMEOUT = 60 * 1000
    private var baseClient: RestTemplate? = null
    private var gistBaseClient: RestTemplate? = null

    @Synchronized
    fun getBaseClient(): RestTemplate {
        if (baseClient == null) {
            log.info("Creating of restTemplate client.")
            val httpRequestFactory = HttpComponentsClientHttpRequestFactory()
            httpRequestFactory.httpClient = httpClient()
            val converter = MappingJackson2HttpMessageConverter()
            converter.objectMapper = jacksonObjectMapper()
            baseClient = RestTemplate(httpRequestFactory)
            baseClient!!.messageConverters = listOf<HttpMessageConverter<*>>(converter)
        }
        return baseClient as RestTemplate
    }

    @Synchronized
    fun getGistBaseClient(): RestTemplate {
        if (gistBaseClient == null) {
            log.info("Creating of gist restTemplate client.")
            val httpRequestFactory = HttpComponentsClientHttpRequestFactory()
            httpRequestFactory.httpClient = gistHttpClient()
            val converter = MappingJackson2HttpMessageConverter()
            converter.objectMapper = jacksonObjectMapper()
            gistBaseClient = RestTemplate(httpRequestFactory)
            gistBaseClient!!.messageConverters = listOf<HttpMessageConverter<*>>(converter)
        }
        return gistBaseClient as RestTemplate
    }

    private fun httpClient(): CloseableHttpClient {
        val requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(TIMEOUT)
                .setConnectTimeout(TIMEOUT)
                .setSocketTimeout(TIMEOUT)
                .build()
        return HttpClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .build()
    }

    private fun gistHttpClient(): CloseableHttpClient {
        val requestConfig = RequestConfig.custom()
                .setAuthenticationEnabled(true)
                .setConnectionRequestTimeout(TIMEOUT)
                .setConnectTimeout(TIMEOUT)
                .setSocketTimeout(TIMEOUT)
                .build()
        val headers = ArrayList<Header>()
        headers.add(BasicHeader("Authorization", "token ${getPropertyValue("authToken")}"))
        headers.add(BasicHeader("Content-Type", "application/json"))
        headers.add(BasicHeader("Accept", "application/json"))
        return HttpClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .setDefaultHeaders(headers)
                .build()
    }
}
