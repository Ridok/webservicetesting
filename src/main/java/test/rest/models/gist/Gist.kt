package test.rest.models.gist

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Gist(
    var description: String,
    var files: Map<String, File>,
    @JsonProperty("public") var isPublic: Boolean
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class File(var content: String)

@JsonIgnoreProperties(ignoreUnknown = true)
class GistResponse {
    var id: String? = null
}
